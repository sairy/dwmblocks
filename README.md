# dwmblocks

Modular status bar for dwm written in c, with added support for calling c functions.

[[_TOC_]]

# Requirements

In order to build dwmblocks you need the Xlib header files.

# Installation / usage

To use dwmblocks first run
```shell
make
```
and then install it with
```shell
make clean install
```
(as root, if needed).

After that you can put dwmblocks in your xinitrc or other startup script to have it start with dwm.

# Modifying blocks

The statusbar is made from text output from commandline programs / c functions.

Blocks are added and removed by editing the *config.h* header file.

By default, the *config.h* header file is created the first time you run `make` which copies the default config from *config.def.h*.

This is so you can edit your status bar commands and they will not get overwritten in a future update.

## External scripts

Like with the original dwmblocks, you can write external scripts (in whichever language you prefer) and add them to *config.h*.

For ease of adding scripts, a `PATH` macro is added that, by default expands to the value of `${HOME}/.config/dwmblocks`.

In order to change this value, edit the value of `BLOCKS_DIR` inside *config.mk* and run `make`.

Note that this only changes the value inside *config.def.h*; if you already have a *config.h*, you have to change it manually inside the file.

The source code for the scripts that I use (commented inside config.def.h) can be [found here](https://gitlab.com/sairy/dotfiles/-/tree/main/.config/dwmblocks).

## Calling C functions

Additionally, this build of dwmblocks allows you to call a function instead of an external script.

This can be particularly useful if your more comfortable with the C language, or if you want to take advantage of static variables instead of having to read/write info from/to temp files.

In order to add a c function, write it in a separate .c file, create a block for it in *config.h* and add its name in as the third entry.

The default config already provides an example with the `net_rx` funtion (which might need some tweaking depending on your network interfaces).

Finally include it (or a header) in *dwmblocks.c*, **BEFORE** the
```c
#include "config.h"
```
line; otherwise config.h won't have access to the function.

The c function called can contain/do anything (aside from calling functions from dwmblocks.c), so long as they:

1. Receive no arguments — `(void)`
2. Return a string (`char *`) value.

# License / Credits

The original dwmblocks is licensed under the **ISC** license and so is this fork, out of respect for the author.

All the credits for the original code go to [**torrinfail**](https://github.com/torrinfail), author of the original dwmblocks.

The original project can be found on their **GitHub**, or by following [this link](https://github.com/torrinfail/dwmblocks).
