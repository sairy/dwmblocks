/* vim: set ft=c: */

/* Macro to facilitate adding new scripts */
#define PATH(cmd)   "__BLOCKS_PATH__/" cmd

/* Maximum length for the command / cfunc output */
#define CMDLENGTH       50

/* Modify this file to change what commands output to your statusbar
 * and then run make to recompile dwmblocks
 *
 * If you do not wish to call a c function,
 * mark the third field of the block as `NULL`
 *
 * If a block has both a command and a c function,
 * the command will be ignored in favor of the function
 */
static const Block blocks[] = {
    /* icon */  /* command */       /* c func */    /* interval */  /* signal */
    {"Mem:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g", NULL, 30, 0},
    {"",  "date '+%b %d (%a) %I:%M%p'", NULL,       5,              0 },

    {"󰇚 ",      NULL,               net_rx,         1,              0 },
    /* {"",        PATH("vol.lua"),    NULL,           0,              2 }, */
    /* {"",        PATH("bat.lua"),    NULL,           3,              0 }, */
    /* {"",        PATH("kbd.sh"),     NULL,           5,              1 }, */
    /* {"",        PATH("date.sh"),    NULL,           5,              0 }, */
};

/* sets delimeter between status commands
 * NULL character ('\0') means no delimeter */
static char base_delim[] = " | ";

/* left and right padding delimeters
 * NULL character ('\0') means no padding */
static char leftpad[]  = " ";
static char rightpad[] = "\0";
