#ifndef __MODULE_UTILS_H

#include <stdint.h>

int pscanf(const char *path, const char *restrict fmt, ...);
char *bprintf(const char *restrict fmt, ...);
char *unit_fmt(uintmax_t num, unsigned int base);

#define __MODULE_UTILS_H
#endif /* __MODULE_UTILS_H */
