# dwmblocks version
VERSION = mira-2.0

# Installation path
PREFIX  = /usr/local

# Path to use for dwmblocks scripts
BLOCKS_DIR = ${HOME}/.config/dwmblocks

# X11
X11INC = /usr/include/X11
X11LIB = /usr/lib/X11

# includes and libs
INCS = -I${X11INC}
LIBS = -L${X11LIB} -lX11

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\"
CFLAGS   = -pedantic -Wall -Wno-deprecated-declarations -Os ${INCS} ${CPPFLAGS}
LDFLAGS  = ${LIBS}

# Compiler and linker
CC = cc
