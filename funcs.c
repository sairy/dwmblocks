#include <stdio.h>
#include <string.h>
#include <limits.h>

#ifndef LENGTH
#define LENGTH(X) (sizeof(X) / sizeof (X[0]))
#endif

#if defined (__linux__)
#include <stdint.h>
#endif /* __linux__ */

#include "module-utils.h"

#define NA "n/a"

static const char *net_findactive(const char *, const char *);

/* stolen from slstatus */
char *
net_rx(void)
{
#if defined (__linux__)
    const char *wifi  = "wlp1s0";
    const char *ether = "enp3s0f3u2";

    const int base = 1024;

    uintmax_t oldrx;
    static uintmax_t newrx;
    char path[PATH_MAX];

    oldrx = newrx;

    const char *interface = net_findactive(ether, wifi);

    if (!interface)
        return NA;

    if (snprintf(path, sizeof(path),
                "/sys/class/net/%s/statistics/rx_bytes",
                interface) < 0)
        return NA;

    if (pscanf(path, "%ju", &newrx) != 1)
        return NA;

    if (oldrx == 0)
        return NA;

    return unit_fmt((newrx - oldrx) / 1, base);
#else
    return NA;
#endif /* __linux__ */
}

const char *
net_findactive(const char *net1, const char *net2)
{
    char pfx[] = "/sys/class/net/";
    char sfx[] = "/operstate";

    char st1[256];
    char st2[256];

    if (pscanf(bprintf("%s%s%s", pfx, net1, sfx), "%s", st1) == 1)
        if (!strcmp(st1, "up"))
            return net1;

    if (pscanf(bprintf("%s%s%s", pfx, net2, sfx), "%s", st2) == 1)
        if (!strcmp(st2, "up"))
            return net2;

    return NULL;
}
