# dwmblocks

include config.mk

SRC = dwmblocks.c funcs.c module-utils.c
OBJ = ${SRC:.c=.o}

all: options dwmblocks

options:
	@echo dwmblocks build options:
	@echo "CFLAGS  = ${CFLAGS}"
	@echo "LDFLAGS = ${LDFLAGS}"
	@echo "CC      = ${CC}"
	@echo

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h

config.h:
	sed 's|__BLOCKS_PATH__|$(BLOCKS_DIR)|' config.def.h > $@

dwmblocks: ${OBJ}
	${CC} -o $@ ${OBJ} ${CFLAGS} ${LDFLAGS}

clean:
	rm -f *.o dwmblocks

install: dwmblocks
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f dwmblocks ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/dwmblocks

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/dwmblocks

.PHONY: all options clean install uninstall
