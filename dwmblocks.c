#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

# include <X11/Xlib.h>

#ifdef __OpenBSD__
# define SIGPLUS         SIGUSR1+1
# define SIGMINUS        SIGUSR1-1
#else
# define SIGPLUS         SIGRTMIN
# define SIGMINUS        SIGRTMIN
#endif /* __OpenBSD__ */

/* custom c functions to call in blocks */
#include "funcs.h"

#define LENGTH(X)       (sizeof(X) / sizeof (X[0]))
#define MIN(a, b)       ((a) < (b) ? (a) : (b))
#define STATUSLENGTH    (LENGTH(blocks) * CMDLENGTH + 1 + \
                         LENGTH(leftpad) + LENGTH(rightpad))

typedef struct {
    char *icon;
    char *command;
    char *(*func) (void);
    unsigned int interval;
    unsigned int signal;
} Block;

/* configuration file */
#include "config.h"

/* function declarations */
static void getcmd(const Block *block, char *output);
static void getcmds(int time);
static void getsigcmds(unsigned int signal);
static int  getstatus(char *str, char *last);
static void pstdout(void);
static void setroot(void);
static void setupsignals(void);
static void sighandler(int signum);
#ifndef __OpenBSD__
 static void dummysighandler(int unused);
#endif /* __OpenBSD__ */
static void statusloop(void);
static void termhandler(int unused);

static void help(void);
static void version(void);

/* variables */
static Display *dpy;
static int screen;
static Window root;

static void (*writestatus)(void) = setroot;
static char statusbar[LENGTH(blocks)][CMDLENGTH] = {0};
static char statusstr[2][STATUSLENGTH];

static char *delim = base_delim;
static size_t delim_len = LENGTH(base_delim) - 1;

static int  running = 1;

/*
 * calls function block->func() if it exists and stores output in *output
 * otherwise, opens process *cmd and stores output in *output
 */
void
getcmd(const Block *block, char *output)
{
    size_t len;
    strncpy(output, block->icon, CMDLENGTH - delim_len);

    len = strlen(block->icon);
    if (block->func) {
        strncpy(output+len, block->func(), CMDLENGTH - len - delim_len);
    } else {
        FILE *cmdf;
        if (!(cmdf = popen(block->command, "r")))
            return;
        fgets(output+len, CMDLENGTH - len - delim_len, cmdf);
        pclose(cmdf);
    }

    /* return if block and output are both empty */
    if (!(len = strlen(output)))
        return;

    /* only chop off newline if one is present at the end */
    if (output[len-1] == '\n')
        len--;

    if (delim[0] != '\0') {
        strncpy(output+len, delim, delim_len);
        len += delim_len;
    }

    output[len] = '\0';
}

void
getcmds(int time)
{
    const Block *current;
    for (unsigned int i = 0; i < LENGTH(blocks); i++) {
        current = blocks + i;
        if ((current->interval && (time % current->interval) == 0) || time == -1)
            getcmd(current, statusbar[i]);
    }
}

void
getsigcmds(unsigned int signal)
{
    const Block *current;
    for (unsigned int i = 0; i < LENGTH(blocks); i++) {
        current = blocks + i;
        if (current->signal == signal)
            getcmd(current,statusbar[i]);
    }
}

int
getstatus(char *str, char *last)
{
    strcpy(last, str);
    str[0] = '\0';
    if (leftpad[0] != '\0')
        strcat(str, leftpad);
    for (unsigned int i = 0; i < LENGTH(blocks); i++)
        strcat(str, statusbar[i]);
    str[strlen(str)-delim_len] = '\0';
    if (rightpad[0] != '\0')
        strcat(str, rightpad);

    /* 0 if they are the same */
    return strcmp(str, last);
}

void
pstdout(void)
{
    /* Only write out if text has changed. */
    if (!getstatus(statusstr[0], statusstr[1]))
        return;
    puts(statusstr[0]);
    fflush(stdout);
}

void
setroot(void)
{
    /* Only set root if text has changed. */
    if (!getstatus(statusstr[0], statusstr[1]))
        return;
    XStoreName(dpy, root, statusstr[0]);
    XFlush(dpy);
}

void
setupsignals(void)
{
#ifndef __OpenBSD__
    /* initialize all real time signals with dummy handler */
    for (int i = SIGRTMIN; i <= SIGRTMAX; i++)
        signal(i, dummysighandler);
#endif /* __OpenBSD__ */

    for (unsigned int i = 0; i < LENGTH(blocks); i++)
        if (blocks[i].signal > 0)
            signal(SIGMINUS+blocks[i].signal, sighandler);
}

void
sighandler(int signum)
{
    getsigcmds(signum-SIGPLUS);
    writestatus();
}

#ifndef __OpenBSD__
/* this signal handler should do nothing */
void
dummysighandler(int unused)
{
    return;
}
#endif /* __OpenBSD__ */

void
statusloop(void)
{
    int i = 0;
    setupsignals();
    getcmds(-1);

    while (running) {
        getcmds(i++);
        writestatus();
        sleep(1.0);
    }
}

void
termhandler(int unused)
{
    running = 0;
}

void
help(void)
{
    puts("usage: dwmblocks [-hvp] [-d delim]");
    exit(EXIT_SUCCESS);
}
void
version(void)
{
    puts("dwmblocks-"VERSION);
    exit(EXIT_SUCCESS);
}

int
main(int argc, char **argv)
{
    for (int i = 0; i < argc; i++) {
        if (!strcmp("--", argv[i]))
            break;
        else if (!strcmp("-h", argv[i]))
            help();
        else if (!strcmp("-v", argv[i]))
            version();
        else if (!strcmp("-p", argv[i]))
            writestatus = pstdout;
        else if (!strcmp("-d", argv[i])) {
            delim = argv[++i];
            delim_len = strlen(delim);
        }
    }

    /* X setup */
    if (!(dpy = XOpenDisplay(NULL))) {
        fprintf(stderr, "dwmblocks: Failed to open display\n");
        return EXIT_FAILURE;
    }
    screen = DefaultScreen(dpy);
    root = RootWindow(dpy, screen);

    signal(SIGTERM, termhandler);
    signal(SIGINT, termhandler);
    statusloop();

    XCloseDisplay(dpy);
    return EXIT_SUCCESS;
}
