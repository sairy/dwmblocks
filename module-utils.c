#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>

#include "module-utils.h"

#ifndef NA
#define NA "n/a"
#endif

#ifndef LENGTH
#define LENGTH(X) (sizeof(X) / sizeof(X[0]))
#endif

char buf[1024];

int
pscanf(const char *path, const char *restrict fmt, ...)
{
    int n;

    FILE *f;
    va_list ap;

    if (!(f = fopen(path, "r")))
        return -1;

    va_start(ap, fmt);
    n = vfscanf(f, fmt, ap);
    va_end(ap);
    fclose(f);

    return n == EOF ? -1 : n;
}

char *
bprintf(const char *restrict fmt, ...)
{
    va_list ap;
    int i;

    va_start(ap, fmt);
    i = vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);

    return i < 0 ? NA : buf;
}

char *
unit_fmt(uintmax_t num, unsigned int base)
{
    const int default_base = 1024;
    int i, pfx_len;
    double scaled;

    const char **pfx;
    const char *pfx_1000[] = {
        "", "k", "M", "G", "T", "P", "E", "Z", "Y"
    };
    const char *pfx_1024[] = {
        "", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi", "Yi"
    };

    if (base == 1000) {
        pfx = pfx_1000;
        pfx_len = LENGTH(pfx_1000);
    } else {
        base = default_base;
        pfx = pfx_1024;
        pfx_len = LENGTH(pfx_1024);
    }

    scaled = num;

    for (i = 0; i < pfx_len && scaled >= base; i++)
        scaled /= base;

    return bprintf("%.1f %sB/s", scaled, pfx[i]);
}
